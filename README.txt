// $Id: README.txt,v 1.1.2.12 2011/01/11 08:46:40 jcmc Exp $

Gallery Assist
--------------

A comprehensive guide to the installation and to using Gallery Assist is available as screencasts at:

http://www.assist-series.com/d6


Incompatibilities
-----------------
* I would be very grateful if you give us feedback in case you get errors or find issues.
I thank you in advance.


Maintainer
----------
The Gallery Assist module was originally developped by:
Juan Carlos Morejon Caraballo

Current maintainer:
Juan Carlos Morejon Caraballo
